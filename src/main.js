import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import vuetify from "./plugins/vuetify";
import "@babel/polyfill";
import Datepicker from "vuejs-datepicker";
// import VCalendar from "v-calendar";

// Vue.use(VCalendar);

Vue.config.productionTip = false;

// Vue.use(Vuetify);

new Vue({
  router,
  vuetify,
  Datepicker,
  render: (h) => h(App),
}).$mount("#app");
